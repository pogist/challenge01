//
//  Aula1Fase1ViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 05/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "Aula1Fase1ViewController.h"

@interface Aula1Fase1ViewController ()

@end

@implementation Aula1Fase1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Continue:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueLicao2Fase1" sender:self];
}


@end
