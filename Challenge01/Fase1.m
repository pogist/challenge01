//
//  Fase1.m
//  Challenge01
//
//  Created by Vitor Muniz on 06/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "Fase1.h"

@implementation Fase1

-(instancetype)initWithNumeroDoExercicio: (int) numeroDoExercicio{
    self = [super init];
    if(self) {
        
        self.notasDeReferencia = [NSMutableArray array];
        self.botaoPorNota = [NSMutableArray array];
        self.opcoesErradas = [NSMutableArray array];
        self.respostasExerciciosDeSom = [[NSMutableArray alloc] init];

        
        if (numeroDoExercicio == 1) {
            [self sortearNumeros: 7];
            [self sortearExercicios: 7];
        }else if (numeroDoExercicio == 2){
            [self sortearNumeros: 5];
            [self sortearExercicios: 5];
        }
        
        
        
        NSLog(@"%@",self.respostasExerciciosDeSom);

    }
    return self;
}

-(void) sortearNumeros: (int)quantNumeros {
    
    int i, contador=0;
    NSString *nro;
    NSMutableSet* listaDeSorteio = [[NSMutableSet alloc] init];
 
    while (listaDeSorteio.count < quantNumeros) {
        i = arc4random_uniform(quantNumeros);
        nro = [NSString stringWithFormat:@"%d",i];
        [listaDeSorteio addObject: nro];
        if (listaDeSorteio.count > contador) {
            contador++;
            [self.respostasExerciciosDeSom addObject:nro];
        }
        
    }

}

-(void) sortearExercicios: (int)quantNumeros {
    //[3,4,5,2,1,0,6]
    //[[1,3,2],[[6][7]]]
    NSString *nro;
    NSString *notaDeReferancia;
    NSMutableSet *opcoes = [[NSMutableSet alloc] init];
    NSMutableSet *posicoes = [[NSMutableSet alloc] init];

    
    int i;
    for ( int j=0; j<quantNumeros ; j++) {
        
        [opcoes removeAllObjects];
        [posicoes removeAllObjects];
        
        while (notaDeReferancia == nil) {
            i = arc4random() % 7;
            if(i != [self.respostasExerciciosDeSom[j] intValue]){
                notaDeReferancia = [NSString stringWithFormat:@"%d",i];
            }
        }
        
        while (opcoes.count < 2) {
            i = arc4random() % quantNumeros;
            if(i!= ([self.respostasExerciciosDeSom[j] intValue]) && (i != [notaDeReferancia intValue])){
                nro = [NSString stringWithFormat:@"%d",i];
                [opcoes addObject: nro];
            }
        }
        
        while (posicoes.count < 3) {

            i = arc4random() % 3;
            nro = [NSString stringWithFormat:@"%d",i];
            [posicoes addObject: nro];

        }
        
        
        
        [self.notasDeReferencia addObject:notaDeReferancia];
        [self.opcoesErradas addObject:[opcoes allObjects]];
        [self.botaoPorNota addObject:[posicoes allObjects]];
 
        notaDeReferancia = nil;
    }
}





@end
