//
//  ExercicioCapturaDeSom1ViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 09/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "ExercicioCapturaDeSom1ViewController.h"

static vDSP_Length const FFTViewControllerFFTWindowSize = 4096;

@interface ExercicioCapturaDeSom1ViewController ()

@end

@implementation ExercicioCapturaDeSom1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   //ainda vamos inplementar
//    self.mpVolumeView = [[MPVolumeView alloc]init];
//    [self.mpVolumeView setShowsVolumesSlider:YES];
    
    
    
    self.microphone = [EZMicrophone microphoneWithDelegate:self];
    self.fft = [EZAudioFFTRolling fftWithWindowSize:FFTViewControllerFFTWindowSize
                                         sampleRate:self.microphone.audioStreamBasicDescription.mSampleRate
                                           delegate:self];
    
    //configurações layout (buttons/labels)
   
    self.fase1 = [[Fase1 alloc] initWithNumeroDoExercicio:self.numeroDaFase];
                  
    self.nomeNotas = @[@"C", @"D", @"E", @"F", @"G", @"A", @"B"];
    self.nomeNotasEscritas = @[@"Dó",@"Ré",@"Mi",@"Fá",@"Sol",@"Lá",@"Si"];
    
    self.nomeNotasSuten = @[@"C#", @"D#", @"F#", @"G#", @"A#"];
    self.nomeNotasEscritasSuten = @[@"Dó #",@"Ré #",@"Fá #",@"Sol #",@"Lá #"];
    
    [self.botaoTenteNovamente setHidden: YES];
    
    self.notaAtual = @"";
    self.indice = 0;
    
    self.indexRespostaCerta = [[self.fase1.respostasExerciciosDeSom objectAtIndex:0] intValue];
    [self.buttonContinuar setHidden:YES];
    if (self.numeroDaFase == 1) {
        self.respostaCerta= [self.nomeNotas objectAtIndex:self.indexRespostaCerta];
        [self.buttonNoteReferenceOutlet setTitle:[NSString stringWithFormat:@"Escute a nota %@",[self.nomeNotasEscritas objectAtIndex:[[self.fase1.respostasExerciciosDeSom objectAtIndex:self.indice] intValue]]] forState:UIControlStateNormal];
        self.reproducaoAudio = [[ReproducaoAudio alloc] initWithArray:self.nomeNotas andNumeroDaOitava:@"2"];
    }else{
        self.respostaCerta= [self.nomeNotasSuten objectAtIndex:self.indexRespostaCerta];
        [self.buttonNoteReferenceOutlet setTitle:[NSString stringWithFormat:@"Escute a nota %@",[self.nomeNotasEscritasSuten objectAtIndex:[[self.fase1.respostasExerciciosDeSom objectAtIndex:self.indice] intValue]]] forState:UIControlStateNormal];
        self.reproducaoAudio = [[ReproducaoAudio alloc] initWithArray:self.nomeNotasSuten andNumeroDaOitava:@"2"];
    }
    

    self.labelNotaCerta.text = [[NSString alloc] initWithString:self.respostaCerta];
    
    
    self.cont = 0;
    self.numErros = 0;
    self.numAcertos = 0;
    
    //valor inicial da string nota, para que nao seja nula ao comparar
    self.noteName = @"-1";
    
    //start no microfone
    [self.microphone startFetchingAudio];
    self.isFetchingAudio = YES;

    //star na função compararNotas;
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(compararNotas) userInfo:nil repeats:YES];
    
    //Plot time
    self.audioPlotTime.plotType = EZPlotTypeBuffer;
    self.output = [[EZAudioDevice alloc] init];
    //self.output = [EZOutput outputWithDataSource:self];
    [EZOutput sharedOutput].delegate = self;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//------------------------------------------------------------------------------
#pragma mark - EZMicrophoneDelegate
//------------------------------------------------------------------------------

-(void)    microphone:(EZMicrophone *)microphone
     hasAudioReceived:(float **)buffer
       withBufferSize:(UInt32)bufferSize
 withNumberOfChannels:(UInt32)numberOfChannels
{
    //
    // Calculate the FFT, will trigger EZAudioFFTDelegate
    //
    [self.fft computeFFTWithBuffer:buffer[0] withBufferSize:bufferSize];
    
    
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.audioPlotTime updateBuffer:buffer[0]
                              withBufferSize:bufferSize];
    });
}

//------------------------------------------------------------------------------
#pragma mark - EZAudioFFTDelegate
//------------------------------------------------------------------------------

- (void)        fft:(EZAudioFFT *)fft
 updatedWithFFTData:(float *)fftData
         bufferSize:(vDSP_Length)bufferSize
{
    float maxFrequency = [fft maxFrequency];
    self.noteName = [[NSString alloc] initWithString:[EZAudioUtilities noteNameStringForFrequency:maxFrequency
                                                        includeOctave:NO]];
    
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.noteLabel.text = [NSString stringWithFormat:@"Note: %@ - Frequency: %.2f", self.noteName, maxFrequency];
        //[weakSelf.audioPlotFreq updateBuffer:fftData withBufferSize:(UInt32)bufferSize];
    });
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (IBAction)retornarAoMenu:(UIButton *)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)compararNotas{
    if (![self.noteName isEqualToString:@"-1"] && self.isFetchingAudio) {
        if ([self.notaAtual isEqualToString:self.noteName]){
            self.cont ++;
        } else {
            self.cont = 0;
            self.notaAtual = [[NSString alloc] initWithString: self.noteName];
        }
        
        if (self.cont == 200) {
            
            [self.buttonContinuar setHidden:NO];
            
            if([self.respostaCerta isEqualToString:self.notaAtual]){
                self.numAcertos++;
                
                [self.microphone stopFetchingAudio];
                self.buttonContinuar.backgroundColor = [UIColor colorWithRed:32.0/255.0 green:176.0/255.0 blue:87.0/255.0 alpha:1.0];
                [self.buttonContinuar setTitle: @"Você acertou! Continue!" forState:UIControlStateNormal];
                
            }else {
                [self.botaoTenteNovamente setHidden:NO];
                self.numErros ++;
                
                self.buttonContinuar.backgroundColor = [UIColor colorWithRed: 227.0/255.0 green:69.0/255.0 blue:34.0/255.0 alpha:1.0];
                if ([self.nomeNotas containsObject:self.notaAtual]) {
                    int indice = (int)[self.nomeNotas indexOfObject:self.notaAtual];
                    [self.buttonContinuar setTitle:[NSString stringWithFormat:@"Errado. Você cantou a nota %@. Continue.", [self.nomeNotasEscritas objectAtIndex:indice]] forState:UIControlStateNormal];

                }else{
                    int indice = (int)[self.nomeNotasSuten indexOfObject:self.notaAtual];
                    [self.buttonContinuar setTitle:[NSString stringWithFormat:@"Errado. Você cantou um: %@. Continue.", [self.nomeNotasEscritasSuten objectAtIndex:indice]] forState:UIControlStateNormal];
                }
                [self.microphone stopFetchingAudio];
            }
           
        }
    }
}

- (IBAction)continuarAction:(UIButton *)sender {
    self.indice ++;
    [self.botaoTenteNovamente setHidden: YES];
    
    if(((self.indice == 7) && (self.numeroDaFase == 1))||((self.indice == 5) &&(self.numeroDaFase == 2))){
        //desativar botao somente para teste
        [self performSegueWithIdentifier:@"segueResultado" sender:self];
        
    } else{
        [self.buttonContinuar setHidden:YES];
        [self.buttonContinuar setTitle:@"" forState:UIControlStateNormal];
        self.indexRespostaCerta = [[self.fase1.respostasExerciciosDeSom objectAtIndex:self.indice] intValue];
        
        if(self.numeroDaFase == 1){
            
            self.respostaCerta= [self.nomeNotas objectAtIndex:self.indexRespostaCerta];
            [self.buttonNoteReferenceOutlet setTitle:[NSString stringWithFormat:@"Escute a nota %@",[self.nomeNotasEscritas objectAtIndex: self.indexRespostaCerta]] forState:UIControlStateNormal];
        }else{
    
            self.respostaCerta= [self.nomeNotasSuten objectAtIndex:self.indexRespostaCerta];
            [self.buttonNoteReferenceOutlet setTitle:[NSString stringWithFormat:@"Escute a nota %@",[self.nomeNotasEscritasSuten objectAtIndex: self.indexRespostaCerta]] forState:UIControlStateNormal];
        }
        [self.microphone startFetchingAudio];
        self.labelNotaCerta.text = [[NSString alloc] initWithString:self.respostaCerta];
       
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString: @"segueResultado"]) {
        ModalResultadoViewController *modalResultado = [segue destinationViewController];
        NSLog(@"%d, %d", self.numAcertos, self.numErros);
        modalResultado.numAcertos = self.numAcertos;
        modalResultado.numErros = self.numErros;
    }
    
}


- (IBAction)tentarNovamente:(UIButton *)sender {
    
    [self.microphone startFetchingAudio];
    [self.buttonContinuar setHidden:YES];
    [self.buttonContinuar setTitle:@"" forState:UIControlStateNormal];
    [self.botaoTenteNovamente setHidden: YES];
}

- (IBAction)buttonNoteReference:(UIButton *)sender {
    
    NSLog(@"entrou");
    
  //  self.output =[EZAudioDevice currentOutputDevice];
   // NSLog(@"%@", self.output.name);

  //  if([self.output.name isEqualToString: @"Fones de Ouvido"]){
    
  //      [self.reproducaoAudio playAudio:self.indexRespostaCerta];
        
 //   }else if ([self.output.name isEqualToString: @"Alto-falante"]){
        
     //   NSLog(@"entrou");
        
        sender.enabled = NO;

        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        
        self.isFetchingAudio = NO;
        [self.microphone stopFetchingAudio];
        
        [self.reproducaoAudio playAudio:self.indexRespostaCerta];
        
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.reproducaoAudio resetAudio:self.indexRespostaCerta];
            [self.microphone startFetchingAudio];
            self.isFetchingAudio = YES;
            sender.enabled = YES;
        });

}


@end
