//
//  LicoesViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 04/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "LicoesViewController.h"
#import "CustomTableViewCell.h"


@interface LicoesViewController ()

@end

@implementation LicoesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //configurações microfone (FFT)
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *error;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error];
    if (error)
    {
        NSLog(@"Error setting up audio session category: %@", error.localizedDescription);
    }
    [session setActive:YES error:&error];
    if (error)
    {
        NSLog(@"Error setting up audio session active: %@", error.localizedDescription);
    }
    
    // Do any additional setup after loading the view.
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.imagensBotoes = [[NSMutableArray alloc] init];
 //   [self carregarImagens];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

//-(void)carregarImagens{
//    
//    NSFileManager* manager=[NSFileManager new];
//    NSBundle* bundle= [NSBundle mainBundle];
//    NSDirectoryEnumerator* enumerator= [manager enumeratorAtPath: [bundle bundlePath] ];
//    for(NSString* path in enumerator)
//    {
//        if([path hasSuffix: @"bloqueada.png"])
//        {
//            [self.imagensBotoes addObject: path];
//        }
//    }
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CustomTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell) {
        
        if (indexPath.row == 0) {
            cell.imagem.image = [UIImage imageNamed:@"Fase 1.png"];
            cell.descricao.text = @"Primeiro contato com as notas musicais!";
            
        }else if (indexPath.row == 1){
            cell.descricao.text = @"Treine o que você aprendeu reconhecendo o som das notas!";
            cell.imagem.image = [UIImage imageNamed:@"Fase 2.png"];
            
        }else if (indexPath.row == 2){
            cell.descricao.text = @"Agora conheça as notas com sustenido e bemol!";
            cell.imagem.image = [UIImage imageNamed:@"Fase 3.png"];
            
        }else if (indexPath.row == 3){
            cell.descricao.text = @"Treine o que você aprendeu reconhecendo o som das notas sustenido e bemol";
            cell.imagem.image = [UIImage imageNamed:@"Fase 4.png"];
            
        }else if (indexPath.row == 4){
            cell.descricao.text = @"Aprenda outra representação das notas musicais";
            cell.imagem.image = [UIImage imageNamed:@"Fase 5.png"];
            
        }else if (indexPath.row == 5){
            cell.descricao.text = @"Exercite sua memória sobre a representação das notas musicais";
            cell.imagem.image = [UIImage imageNamed:@"Fase 6.png"];
            
        }else if (indexPath.row == 6){
            cell.descricao.text = @"Treine o que você aprendeu e tente cantar o som das notas naturais indicadas";
            cell.imagem.image = [UIImage imageNamed:@"Fase 7.png"];
            
        }else if (indexPath.row == 7){
            cell.descricao.text = @"Agora treine sua capacidade de cantar as notas corretas incluindo notas com sustenido e bemol";
            cell.imagem.image = [UIImage imageNamed:@"Fase 8.png"];
            
         }
    }
    return cell;
}

-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
    if(indexPath.row == 0){
        [self performSegueWithIdentifier:@"segueFase1" sender:self];
    }else if(indexPath.row == 1){
        [self performSegueWithIdentifier:@"segueExercicioDeSom" sender:@"1"];
    }else if(indexPath.row == 2){
        [self performSegueWithIdentifier:@"segueFase3" sender:self];
    }else if(indexPath.row == 3){
        [self performSegueWithIdentifier:@"segueExercicioDeSom" sender:@"2"];
    }else if(indexPath.row == 4){
        [self performSegueWithIdentifier:@"segueFase5" sender:self];
    }else if(indexPath.row == 5){
        [self performSegueWithIdentifier:@"segueFase6" sender:self];
    }else if(indexPath.row == 6){
        [self performSegueWithIdentifier:@"segueFase7" sender:@"1"];
    }else if (indexPath.row == 7){
        [self performSegueWithIdentifier:@"segueFase7" sender:@"2"];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"segueExercicioDeSom"]) {
        Exercicio1SonsViewController *exercicioViewController = [segue destinationViewController];
        if ([sender isEqualToString:@"1"]) {
            exercicioViewController.numeroDaFase = 1;
        } else{
            exercicioViewController.numeroDaFase = 2;
        }
    }else if([segue.identifier isEqualToString:@"segueFase7"]){
        ExercicioCapturaDeSom1ViewController *exercicioCapturaDeSom = [segue destinationViewController];
        if([sender isEqualToString:@"1"]){
            exercicioCapturaDeSom.numeroDaFase = 1;
        }else{
            exercicioCapturaDeSom.numeroDaFase = 2;
        }
        
    }

}



@end
