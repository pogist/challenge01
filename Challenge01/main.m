//
//  main.m
//  Challenge01
//
//  Created by Vitor Muniz on 03/05/16.
//  Copyright © 2016 Vitor Muni/Users/vitormuniz/Desktop/Challenge01/Challenge01/Main.storyboardz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
